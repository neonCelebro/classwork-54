import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Cards from './Carts/Cards.js';
import './Carts/Cards.css';

class App extends Component {
  rank = [2, 3, 4, 5, 6, 7, 8, 9, 10, 'j', 'q', 'k', 'a'];
  suits = ['D', 'C', 'S', 'H'];
  state = {
    cards: [],
    combo: null,
  };
  randomRank = () => this.rank[Math.floor(Math.random() * (13 - 0) + 0)];
  randomSuit = () => this.suits[Math.floor(Math.random() * (4 - 0) + 0)];
  addRandomCards = () => {
    let cards = [];
    for (var i = 0; i < 5; i++) {
      let Card = {
        rank : this.randomRank(),
        suit: this.randomSuit(),
      };
      cards.map((key) => {
        while (JSON.stringify(key) === JSON.stringify(Card)) {
          console.log(JSON.stringify(key) + ' и ' + JSON.stringify(Card) +
        (JSON.stringify(key) === JSON.stringify(Card)));
            Card.rank = this.randomRank();
            Card.suit = this.randomSuit();
        }
      });
      cards.push(Card);
    };
    this.setState({cards});
  };

  cardsCombo = () => {
    let rank = [];
    let sortRank = [];
    let suit = [];
    this.state.cards.map((key) => {
      rank.push(key.rank);
      suit.push(key.suit);
    })
    rank.map((key) => {
      switch (key) {
        case 'j':
          key = 11;
          sortRank.push(key);
        break;
        case 'q':
          key = 12;
          sortRank.push(key);
        break;
        case 'k':
          key = 13;
          sortRank.push(key);
        break;
        case 'a':
        key = 14;
        sortRank.push(key);
        break;
        default:
        sortRank.push(key);
      }
    });
    sortRank = sortRank.sort((a, b) => a - b);
    for (var i = 0; i < sortRank.length; i++) {
      if ((sortRank[0] === 10) &&
          (sortRank[1] === 11) &&
          (sortRank[2] === 12) &&
          (sortRank[3] === 13) &&
          (sortRank[4] === 14) &&
          (suit[0] === suit[1]) &&
          (suit[0] === suit[2]) &&
          (suit[0] === suit[3]) &&
          (suit[0] === suit[4])) {
            this.setState({combo : 'роял - флеш'});
            break;
      }
      if (((sortRank[0] === (sortRank[1] - 1)) &&
          (sortRank[0] === (sortRank[2] - 2)) &&
          (sortRank[0] === (sortRank[3] - 3)) &&
          (sortRank[0] === (sortRank[4] - 4)) ||
         ((sortRank[0] === 2) &&
          (sortRank[1] === 3) &&
          (sortRank[2] === 4) &&
          (sortRank[3] === 5)) &&
          (sortRank[4] === 14)) && (
      (suit[i] === suit[i+1]) &&
      (suit[i] === suit[i+2]) &&
      (suit[i] === suit[i+3]) &&
      (suit[i] === suit[i+4])))
    {
      this.setState({combo : 'стрит - флеш'});
      break;
    }
      if ((sortRank[i] === (sortRank[i+1])) &&
          (sortRank[i] === (sortRank[i+2])) &&
          (sortRank[i] === (sortRank[i+3])))
          {
            this.setState({combo : 'каре'});
            break;
      }
      if (((sortRank[0] === (sortRank[1])) &&
          (sortRank[0] === (sortRank[2])) &&
          (sortRank[3] === (sortRank[4])) ||
        ((sortRank[0] === (sortRank[1])) &&
          (sortRank[2] === (sortRank[3])) &&
          (sortRank[2] === (sortRank[4]))))) {
            this.setState({combo : 'фул-хаус'});
            break;
      }
      if ((suit[0] === suit[1]) &&
          (suit[0] === suit[2]) &&
          (suit[0] === suit[3]) &&
          (suit[0] === suit[4])) {
            this.setState({combo : 'флеш'});
            break;
      }
      if (((sortRank[0] === (sortRank[1] - 1)) &&
          (sortRank[0] === (sortRank[2] - 2)) &&
          (sortRank[0] === (sortRank[3] - 3)) &&
          (sortRank[0] === (sortRank[4] - 4))) ||
        ((sortRank[0] === 2) &&
          (sortRank[1] === 3) &&
          (sortRank[2] === 4) &&
          (sortRank[3] === 5) &&
          (sortRank[4] === 14)))
          {
          this.setState({combo : 'стрит'});
          break;
      }
      if ((sortRank[i] === sortRank[i+1])
      && (sortRank[i] === sortRank[i+2])){
          this.setState({combo : 'тройка'});
          break;
      }if (sortRank[i] === sortRank[i+1]) {
          this.setState({combo : 'пара'});
          break;
      } else {
        this.setState({combo : 'ничего'});
      }
    };
    console.log(sortRank);
    console.log(suit);
  };
  render() {
    return (
      <div className="App playingCards">
      <button className='addCards' onClick={this.addRandomCards}>Разадть карты</button>
      <button className='addCards' onClick={this.cardsCombo}>Моя комбинация</button>
      <div>{this.state.combo}</div>
      {
        this.state.cards.map((cards, index) => {
          return <Cards suit={cards.suit} rank={cards.rank} key={index}></Cards>
        })
      }
      </div>
    )
  };
};

export default App;
