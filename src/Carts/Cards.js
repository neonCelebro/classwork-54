import React, { Component } from 'react';

const Cards = (props) => {
  let suitClass = '';
  let symbol = '';
  switch (props.suit.toString()) {
    case 'D':
      suitClass = 'diams';
      symbol = '♦';
      break;
    case 'H':
      suitClass = 'hearts';
      symbol = '♥';
      break;
    case 'C':
      suitClass = 'clubs';
      symbol = '♣';
      break;
    case 'S':
      suitClass = 'spades';
      symbol = '♠';
      break;
  }

  const cardClassName = 'card rank-' + props.rank.toString() + ' ' + suitClass;
  return (

  <div className={cardClassName}>
    <span className="rank">{props.rank.toString().toUpperCase()}</span>
    <span className="suit">{symbol}</span>
  </div>
);
}
export default Cards;
